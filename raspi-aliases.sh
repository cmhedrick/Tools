alias act='source env/bin/activate'

# update everything quickly
alias quickUpdate="sudo apt update && sudo apt dist-upgrade -y && sudo apt autoremove -y"

# turn off IPv6 for crappy VPN
alias ipv6Kill="sudo sh -c 'echo 1 > /proc/sys/net/ipv6/conf/all/disable_ipv6'"
alias ipv6Restore="sudo sh -c 'echo 0 > /proc/sys/net/ipv6/conf/all/disable_ipv6'"
alias getPorts='netstat -tulpn'

# get packets
alias startmon11="sudo airmon-ng start wlan1 11"
alias getGETPackets="sudo tshark -i wlan1mon -Y \"http.request.method == \"GET\"\" | tee `(date +"%m_%d_%Y_%H%M%S")`_get_packets.pcap"
alias getAllPackets="sudo tshark -i wlan1mon | tee `(date +"%m_%d_%Y_%H%M%S")`_all_packets.pcap" 