# yes I use VIM
export EDITOR=vim
# update everything quickly
alias quickUpdate="sudo apt update && sudo apt dist-upgrade -y && sudo apt autoremove -y && sudo snap refresh && sudo update-initramfs -u"
# turn off IPv6 for crappy VPN
alias ipv6Kill="sudo sh -c 'echo 1 > /proc/sys/net/ipv6/conf/all/disable_ipv6'"
alias ipv6Restore="sudo sh -c 'echo 0 > /proc/sys/net/ipv6/conf/all/disable_ipv6'"
# working with git
alias s='git status'
alias d='git diff HEAD | colordiff | less -R'
# django development
alias act='source env/bin/activate'
alias rs='python manage.py runserver 0.0.0.0:8000'
# init raspberry pi connection
alias raspi='sudo screen /dev/ttyUSB0 115200'
# get ports being used
alias getPorts='netstat -tulpn'

alias signify=signify-openbsd
alias fastboot=/home/user/platform-tools/fastboot
alias getPyIgnore="wget https://gitlab.com/-/snippets/2046885/raw/master/.gitignore"

# dirs with specific bins
#PATH=$PATH:/home/$USER/android-studio/platform-tools
PATH=$PATH:/home/$USER/node/bin
PATH=$PATH:node_modules/.bin

PATH=$PATH:$HOME/.poetry/bin
PATH=$PATH:/home/user/android-studio/bin
PATH=$PATH:/home/user/snap/flutter/common/flutter/bin
PATH=$PATH:/home/user/platform-tools

if [ -d "$HOME/platform-tools" ] ; then
 export PATH="$HOME/platform-tools:$PATH"
fi
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
if command -v pyenv 1>/dev/null 2>&1; then
 eval "$(pyenv init -)"
fi
