#! /bin/bash
# ensure deb-install-docker.sh is in the same directory
# Core Installs:
# Git, Aircrack-ng, VIM, Virtualenv, 
# CURL, G-PARTED, HEXChat, pwgen, WireShark
# nmap, MACChanger, cewl (wordlist creator)

# Plugin Installs:
# Colordiff (colorizes git changes using git status), Python Dev packages 
# (assists in building packages),
# Libraries to support pillow and image types.
# secure-delete for all your secure deleting needs

# Installer Scripts: 
# - deb-install-docker.sh: install docker

sudo apt install git aircrack-ng nmap vim colordiff virtualenvwrapper \
    python3-dev libtiff5-dev libjpeg8-dev zlib1g-dev libfreetype6-dev \
    liblcms2-dev libwebp-dev libharfbuzz-dev libfribidi-dev tcl8.6-dev \
    tk8.6-dev curl gparted hexchat pwgen wireshark macchanger \
    secure-delete openvpn network-manager-openvpn \
    network-manager-openvpn-gnome cewl libsqlite3-dev

# install docker and all fix-ins
./deb-install-docker.sh

# snap installs (though I hate snap...)
sudo snap install code --classic