import os
import socket
import tempfile
import ipaddress
import socketserver
import http.server


def main():
    host = ipaddress.IPv4Address("0.0.0.0")

    serving_dir = os.path.join(
        tempfile._get_default_tempdir(), next(tempfile._get_candidate_names())
    )

    # create the serving directory
    serve_path = os.path.join(serving_dir, "www")
    os.makedirs(serve_path)
    serve_http()


class ReuseTCPServer(socketserver.TCPServer):
    def server_bind(self):
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind(self.server_address)


class Handler(http.server.SimpleHTTPRequestHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def log_message(self, format, *func_args):
        super().log_message(format, *func_args)

    def log_request(self, format, *func_args):
        super().log_request(format, *func_args)


def serve_http():
    with ReuseTCPServer(("", 80), Handler) as httpd:
        httpd.serve_forever()


if __name__ == "__main__":
    main()
